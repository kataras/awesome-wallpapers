# Awesome Wallpapers

I didn't created these, neither I know who, I'm not either a designer but I'm collecting those desktop wallpapers for a long time for **personal use only**. I decided to publish them _somewhere with access from anywhere_.

## Sources

1. https://github.com/pop-os/wallpapers
2. https://github.com/elementary/wallpapers
3. http://wonderfulengineering.com/45-hd-wonderful-wallpapers-for-pc-desktop-and-laptops/
4. https://a2zwallpaper.com/2017/11/windows-desktops-hd-wallpapers/

> That's all I could possible remember right now